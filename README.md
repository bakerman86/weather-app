# Weather App

Weather app for made for Clockwork made with Vue.js, Leaflet and Dark Sky API.

![picture](public/og.jpg)

## Getting Started

``` bash
# clone repo and API submodule
git clone https://bakerman86@bitbucket.org/bakerman86/weather-app.git
```

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run serve

# build for production with minification
npm run build

# Lints and fixes files
npm run lint

# Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
```

## Built With
* [Vue.js](https://vuejs.org/)
* [Dark Sky API](https://darksky.net/dev/)
* [Leaflet](https://leafletjs.com/reference-1.6.0.html)