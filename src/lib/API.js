const proxy = "https://cors-anywhere.herokuapp.com/";
const DARKSKY_API_KEY = "5e8a6e333a1ec896369561f0d147bff6";
const WEATHER_API_URL = `${proxy}https://api.darksky.net/forecast/${DARKSKY_API_KEY}/`;
const GEO_API_URL = `${proxy}https://darksky.net/geo?q=`;
const ADDRESS_API_URL = `${proxy}https://darksky.net/rgeo?hires=1`;
const LANGUAGE = "?lang=nl&units=si";

function getCoordinates(location) {
    return fetch(`${GEO_API_URL}${location}`).then(response => response.json());
}

function getForecast(lat, lng) {
    return fetch(`${WEATHER_API_URL}${lat},${lng}${LANGUAGE}`).then(response =>
        response.json()
    );
}

function getAddress(lat, lng) {
    return fetch(`${ADDRESS_API_URL}&lat=${lat}&lon=${lng}`).then(response =>
        response.json()
    );
}

export default {
    getAddress,
    getForecast,
    getCoordinates
};
