import Vue from "vue";
import App from "./App.vue";
import { BootstrapVueIcons } from "bootstrap-vue";
import VueSkycons from "vue-skycons";
import VueNumber from "vue-number-animation";

import "bootstrap/dist/css/bootstrap.css";
import "leaflet/dist/leaflet.css";

const moment = require("moment");
require("moment/locale/nl");

Vue.use(BootstrapVueIcons);
Vue.use(VueSkycons);
Vue.use(require("vue-moment"), {
    moment
});
Vue.use(VueNumber);

Vue.filter("round", function(value, decimals) {
    if (!value) {
        value = 0;
    }

    if (!decimals) {
        decimals = 0;
    }

    value = Math.round(value * Math.pow(10, decimals)) / Math.pow(10, decimals);
    return value;
});

Vue.filter("toPercentage", function(value) {
    return Math.round(value * 100);
});

new Vue({
    render: h => h(App)
}).$mount("#app");
